DumpDiff

# Purpose:
This is a tool to indentify area of interest in memory dumps from c64 emulators. 

# Say what?

When fiddling with c64 software, I have run into the problem that some things doesn't work after a certain move, like calling a subroutine. If you want to follow up, it's good to identify the delta and see which change was relevant. But just comapring before and after will give you a number of false positives. A lot. Many.

So the idea here is that you grab a few samples before (let's call them "the before files") a change and another few examples after (let's call them "the after files").

The program then identifies the data that is stable(same in all) in the before files and the data that is stable in the after files.

As a last them, the program prints a list of the data that is stable in both the before and after files, and also different.

So, the program prints data stable which differs between the before and after.


# Usage:

Preparation:

Option 1:
Run 64Debugger.  https://sourceforge.net/projects/c64-debugger/

The command CTRL+U dumps 64K of RAM to a file. If we dump the RAM before and after doing a change. 

Option 2:
Run VICE (or some other emulator), enter the monitor and save (s "<filename>" 0 0000 ffff) before and after a change.

We described above, we need multiople files both before anfdte after


How to use the program:

2) Create two textfiles; one textfile with the names of the before files and another textfile with the after files.

For example the text file before.txt contianing
before1.bin
before2.bin
before3.bin

3) Shart a shell in the directory where you have the files (both the dumps and the configuration files) and run the program with the textfiles as input parameters.

Say: 

dumpdiff.exe /conf1 before.txt /conf2 after.txt /v 

This will show the differences on screen.

Or 

dumpdiff.exe /conf1 before.txt /conf2 after.txt >comparefile.txt

This will save the differences to file.
